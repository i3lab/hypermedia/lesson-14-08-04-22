const express = require('express')
const app = express()
const { Sequelize, DataTypes } = require("sequelize")
app.use(express.json())

const database = new Sequelize("postgres://postgres:postgres@localhost:5432/hyp")


// Function that will initialize the connection to the database
async function initializeDatabaseConnection() {
    await database.authenticate()
    const Cat = database.define("cat", {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        breed: DataTypes.STRING,
        img: DataTypes.STRING,
    })
    await database.sync({ force: true })
    return {
        Cat
    }
}

// With this line, our server will know how to parse any incoming request
// that contains some JSON in the body

const pageContentObject = {
    index: {
        title: "Homepage",
        image: "https://fs.i3lab.group/hypermedia/images/index.jpeg",
        description: "Short description of homepage"
    },
    about: {
        title: "About",
        image: "https://fs.i3lab.group/hypermedia/images/about.jpeg",
        description: "Short description of about"
    },
}

// Our fake database 
// const catList = [
//     {
//         id: 1,
//         name: "Cat 1",
//         breed: "Siberian",
//         details: "Details about cat 1",
//         img: "https://fs.i3lab.group/hypermedia/cats/siberian.jpg",
//     },
//     {
//         id: 2,
//         name: "Cat 2",
//         breed: "Birman",
//         details: "Details about cat 2",
//         img: "https://fs.i3lab.group/hypermedia/cats/birman.jpg",
//     },
//     {
//         id: 3,
//         name: "Cat 3",
//         breed: "Bombay",
//         details: "Details about cat 3",
//         img: "https://fs.i3lab.group/hypermedia/cats/bombay.jpg",
//     },
//     {
//         id: 4,
//         name: "Cat 4",
//         breed: "Calico",
//         details: "Details about cat 4",
//         img: "https://fs.i3lab.group/hypermedia/cats/calico.jpg",
//     },
//     {
//         id: 5,
//         name: "Cat 5",
//         breed: "Maine Coon",
//         details: "Details about cat 5",
//         img: "https://fs.i3lab.group/hypermedia/cats/maine-coon.jpg",
//     },
// ]

async function runMainApi() {
    const models = await initializeDatabaseConnection()
    await models.Cat.create(
        {
            name: "Cat 5",
            breed: "Maine Coon",
            description: "Details about cat 5",
            img: "https://fs.i3lab.group/hypermedia/cats/maine-coon.jpg",
        }

    )

    app.get('/page-info/:topic', (req, res) => {
        const { topic } = req.params
        const result = pageContentObject[topic]
        return res.json(result)
    })

    app.get('/cats/:id', async (req, res) => {
        const id = +req.params.id
        const result = await models.Cat.findOne({ where: { id } })
        return res.json(result)
    })

    // HTTP GET api that returns all the cats in our fake database
    app.get("/cats", async (req, res) => {
        const result = await models.Cat.findAll()
        const filtered = []
        for (const element of result) {
            filtered.push({
                name: element.name,
                img: element.img,
                breed: element.breed,
                id: element.id,
            })
        }
        return res.json(filtered)
    })

    // HTTP POST apio that will push (and therefore create) a new element in 
    // our fake database 
    app.post("/cats", (req, res) => {
        const { body } = req
        catList.push(body)
        return res.sendStatus(200)
    })
}

runMainApi()


export default app
